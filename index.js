const express = require('express')
const app = express()
const fs = require('fs');
const csv = require('csv-parser');
const path = require('path')

const openedRestaurant = [], CSVRestaurants = [];
const days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'hbs')

//Route default home
app.get('/', (req, res) => {
    res.render('index',
        {
            title: 'Hello, world!',
            content: 'How are you?'
        }
    )
})

//Route for searching open restaurant
// we pass datetime filter by getParam

app.get('/searching', function (req, res) {

    var opendResturant = []
    var dateTime = req.query.dateTime;
    var date = new Date(dateTime);
    var hour = date.getHours()
    var minute = date.getMinutes()
    var day = date.getDay() === 0 ? days[days.length - 1] : days[date.getDay() - 1]
    CSVRestaurants.map((e) => {

        e.days.map((t) => {
            if (t.day === day) {
                var testTime = hour + ":" + minute;
                if (testTime > t.fromConvert && testTime <= t.toConvert) {
                    opendResturant.push(e.name)

                }

            }
        })
    });
    res.send({status: 200, data: opendResturant});
});

/**
 * Return list of open Restaurants.
 * @param {string} csvFilename The csv file name.
 * @param {object} searchDatetime The Date object.
 * @return {list} The result of adding num1 and num2.
 */
async function findOpenRestaurants(csvFilename, searchDatetime) {


    let promise = new Promise((res, rej) => {
        setTimeout(() => {
            var day = searchDatetime.getDay() === 0 ? days[days.length - 1] : days[searchDatetime.getDay() - 1]
            fs.createReadStream(csvFilename)
                .pipe(csv(['Rest', 'open']))
                .on('data', (data) => {
                    var daysArrOfResturant = []
                    days.map((d) => {
                        daysArrOfResturant.push({
                            day: d,
                            from: "",
                            to: "",
                            fromConvert: null,
                            toConvert: null
                        })
                    })
                    var periods = data.open.split('/');
                    periods.map((e) => {

                        var times = e.substring(e.indexOf(e.match(/\d+/)[0]), e.length).split("-");
                        var daysArray = e.substring(0, e.indexOf(e.match(/\d+/)[0])).split(",");
                        daysArray.map((d) => {
                            if (d.length === 3) {
                                daysArrOfResturant = setObject(daysArrOfResturant, days.indexOf(d), times);
                            } else {
                                d = d.split(" ").join("");
                                var dArray = d.split("-")
                                if (dArray.length > 1) {

                                    var fromIndex = days.indexOf(dArray[0])
                                    var toIndex = days.indexOf(dArray[1])

                                    for (var i = fromIndex; i <= toIndex; i++) {
                                        daysArrOfResturant = setObject(daysArrOfResturant, i, times);
                                    }
                                } else {
                                    daysArrOfResturant = setObject(daysArrOfResturant, days.indexOf(dArray[0]), times);
                                }
                            }
                        })

                    })
                    CSVRestaurants.push({
                        name: data.Rest,
                        days: daysArrOfResturant,
                    })
                    daysArrOfResturant.map((t) => {
                        if (t.day === day) {
                            var testTime = searchDatetime.getHours() + ":" + searchDatetime.getMinutes();
                            if (testTime > t.fromConvert && testTime <= t.toConvert) {
                                openedRestaurant.push(data.Rest)

                            }

                        }
                    })
                })
                .on('end', () => {
                    res(openedRestaurant)

                });
        }, 1000)
    });

    let result = await promise;

    return result

}

/**
 * Return list of days Of Restaurants that getting from csv.
 * @param {array} days list of days.
 * @param {string} day string of day.
 * @return {array} days .
 */
function setObject(days, day, times) {

    days[day].from = times[0].replace(/^\s+|\s+$/g, '')
    days[day].to = times[1].replace(/^\s+|\s+$/g, '')
    days[day].fromConvert = convertTime12to24(days[day].from)
    days[day].toConvert = convertTime12to24(days[day].to)
    return days
}

/**
 * Convert time Ex: 1 am  to 13
 * @param {string} time12h string of time to be converted.
 * @return {string} converted to 24.
 */
function convertTime12to24(time12h) {

    var [time, modifier] = time12h.split(' ');


    if (time.split(':').length > 1) {
        let [hours, minutes] = time.split(':');

        if (hours === '12') {
            hours = '00';
        }

        if (modifier === 'pm') {
            hours = parseInt(hours, 10) + 12;
        }

        return `${hours}:${minutes}`;

    } else {

        var hours = "";
        if (time === '12') {
            time = '00';
        }

        if (modifier === 'pm') {
            time = parseInt(time, 10) + 12;
        }
        return time + ":00";
    }


}

async function callAsync() {

    let promise = new Promise((res, rej) => {
        setTimeout(() => {
            res(findOpenRestaurants("rest_hours.csv", new Date()))
        }, 1000)
    });

    let result = await promise;
    console.log(result)
    return result


}

callAsync()

const port = process.env.PORT || 3001
app.listen(port, () => console.log(`App listening on port ${port}`))
